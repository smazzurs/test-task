## Task details
The goal is to implement a simple one-page application. A user should be able to fill survey form, submit it and view other submitted results. 

This repo is set up with `create-react-app` and `express` server. There are endpoints to retrieve survey results and submit the form. So you should implement only the frontend part.

You could check design wireframe (*wireframe.png*). It's just a wireframe, so feel free to style it as you wish.

Hours spent on this task will be compensated for a candidate, that will be selected for the job.

When finished create pull request or fork.

**Acceptance criteria:**

* Two main sections should be presented
* The first section is a survey form. The form should be always visible. Should contain 4 elements: Name input (text), hometown (select), list of favourite tools (checkboxes) and submit button.
* Backend validation is implemented, so you don’t need to make frontend validation, but the user should see backend validation errors.
* List of options (hometown, favourite tools) is not important, just hardcode some values (e.g. hometown: *London, Manchester, Liverpool, Leeds*; favourite tools: *WebStorm, Atom, Sketch, Photoshop*)
* Second section is a list of survey results. The list should be always visible. Results should be fetched from the endpoint (endpoints are described later in this README). After the user submits the survey form his result should be added to the top of the list.
* Should be implemented using `react` + `redux`
* You could choose any project structure and approach/processor for styling.


**What matters:**

* Clean code/CSS
* Clean folders structure
* Good UI/UX
* It’s not expected to be styled well, so don’t spend much time on it, just add some basic styles. We just want to see how clean are your CSS files.

Since it’s a test task, we do not expect you to spent much time on it. So if the task is finished and you have some ideas about which improvements you could implement - please provide a detailed plan of possible improvements.

## Development

* Install packages with `npm i`
* Run both backend server ( `npm run server` ) and frontend ( `npm start` ). Then you could access frontend on `3000` port and backend on `3001`

## Endpoints

**To retrieve the list of survey results:**

`GET http://localhost:3001/api/pool-results`

**To add user's survey results:**

`POST http://localhost:3001/api/pool-results`

Request body example

```
{
    "name": "Albert",
    "home_town": "Leeds",
    "favorite_tools": ["Photoshop", "Sketch"]
}
```
Results will be added to the list. Backend will respond with added item.