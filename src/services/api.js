import axios from 'axios'
import * as ApiAlerts from '../utils/apiAlerts'

export default class API {
  static postUser (data) {
    return axios('http://localhost:3001/api/pool-results', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      data: {
        favorite_tools: data.favorite_tools,
        name: data.name,
        home_town: data.home_town
      }
    })
      .catch(function (error) {
        return ApiAlerts.ErrorExtractor(error)
      })
  }
  static getUsers () {
    return axios('http://localhost:3001/api/pool-results', {
      method: 'get'
    })
      .catch(function (error) {
        return ApiAlerts.ErrorExtractor(error)
      })
  }
}