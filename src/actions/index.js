import { CHANGE_STATE_PROP, SHOW_MODAL, HIDE_MODAL } from '../constants'

export function changeStateProp (prop, value, reducer) {
  return dispatch => {
    dispatch({
      type: reducer.toUpperCase() + CHANGE_STATE_PROP,
      state: {
        prop: prop,
        value: value
      }
    })
  }
}

export function showModal (data) {
  return dispatch => {
    dispatch({
      type: SHOW_MODAL,
      data
    })
  }
}

export function hideModal () {
  return dispatch => {
    dispatch({
      type: HIDE_MODAL
    })
  }
}