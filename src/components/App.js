import React, { Component } from 'react'
import { Provider } from 'react-redux'

import './App.css'
import FormContainer from './Form/FormContainer'
import ResultsContainer from './Results/ResultsContainer'
import ModalContainer from './Modal/ModalContainer'

class App extends Component {
  render () {
    return (
      <Provider store={this.props.store}>
        <div className="App">
          <FormContainer />
          <ResultsContainer />
          <ModalContainer />
        </div>
      </Provider>
    )
  }
}

export default App
