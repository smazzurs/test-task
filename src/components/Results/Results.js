import React, { PureComponent } from 'react'

import API from '../../services/api'

class Results extends PureComponent {
  componentDidMount () {
    API.getUsers().then((res) => {
      this.props.changeStateProp('users', res.data, 'main')
    })
  }

  renderTile (user, index) {
    return (
      <ul key={index} className='tile'>
        <li>Name: {user.name}</li>
        <li>Home town: {user.home_town}</li>
        <li>Favorite tools: {user.favorite_tools.join(' ')}</li>
      </ul>
    )
  }

  renderResults () {
    let arr = this.props.users
    let newResults = []
    for (let i = arr.length - 1; i >=0 ; i--) {
      newResults.push(
        this.renderTile(arr[i], i)
      )
    }
    return newResults
  }

  render () {
    return (
      <div className='results'>
        <span>Pool results:</span>
        {this.renderResults()}
      </div>
    )
  }
}

export default Results
