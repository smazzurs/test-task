import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Results from './Results'
import { changeStateProp } from '../../actions'

const mapStateToProps = (state) => {
  return {
    users: state.main.users
  }
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ changeStateProp }, dispatch)
}

const ResultsContainer = (connect(
  mapStateToProps,
  mapDispatchToProps
)(Results))

export default ResultsContainer
