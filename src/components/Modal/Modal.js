import React, { PureComponent } from 'react'

import { limitTextLength } from '../../utils/index'

class Modal extends PureComponent {
  componentDidUpdate (prevProps, prevState) {
    if (this.props.modal) document.body.style.overflow = 'hidden'
    else document.body.style.overflow = 'auto'
  }
  getButtons () {
    return (this.props.data.buttons && this.props.data.buttons.length)
      ? (
        this.props.data.buttons.map((obj, i) => {
          return <div className='modal-button custom' key={i}
                      onClick={() => {
                        this.props.hideModal()
                        obj.onPress && obj.onPress()
                      }}>
            {obj.text}
          </div>
        })
      )
      : (
        <div className='modal-button default' onClick={() => { this.props.hideModal() }}>OK</div>
      )
  }
  render () {
    if (this.props.modal) {
      return (
        <div className='app-modal-overlay'>
          <div className='app-modal'>
            {(this.props.data.title !== null) && <p className='title'>{limitTextLength(this.props.data.title, 250) || ''}</p>}
            <p className='message' dangerouslySetInnerHTML={{__html: this.props.data.message}} />
            <div className='buttons'>
              {this.getButtons()}
            </div>
          </div>
        </div>
      )
    } else {
      return (
        <div />
      )
    }
  }
}

export default Modal
