import React, { PureComponent } from 'react'
import PropTypes from 'prop-types'
import Select from 'react-select'
import FontAwesomeIcon from '@fortawesome/react-fontawesome'
import {
  faCaretDown
} from '@fortawesome/fontawesome-free-solid'

const DropdownIndicator = ({ innerRef, innerProps }) => (
  <div
    ref={innerRef}
    className='fg-dropdown__arrow'
    {...innerProps}
  >
    <FontAwesomeIcon icon={faCaretDown} size='lg' />
  </div>
)

class SelectDropdown extends PureComponent {
  handleWarning (warning) {
    if (!this.props.validData && this.props.warningReason) {
      return (
        <div className='warning-text'>* {warning}</div>
      )
    }
  }
  render () {
    const { warningReason } = this.props
    return (
      <div className='fg-select-container'>
        <Select
          className='fg-dropdown-container'
          classNamePrefix='fg-dropdown'
          components={{ DropdownIndicator }}
          {...this.props}
          warningReason={undefined}
          validData={undefined}
        />
        {this.handleWarning(warningReason)}
      </div>
    )
  }
}

SelectDropdown.propTypes = {
  warningReason: PropTypes.string,
  validData: PropTypes.bool.isRequired
}

SelectDropdown.defaultProps = {
  validData: true
}

export default SelectDropdown
