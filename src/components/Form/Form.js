import React, { PureComponent } from 'react'
import update from 'immutability-helper'

import API from '../../services/api'
import SelectDropdown from '../SelectDropdown/SelectDropdown'

const hardcodedOption = ['London', 'Manchester', 'Leeds', 'Liverpool']
const hardcodedTools = ['Atom', 'WebStorm', 'Sketch', 'Photoshop']

class Form extends PureComponent {
  constructor (props) {
    super(props)
    this.state = {
      form: {
        favorite_tools: this.defaultFieldValue('favorite_tools'),
        home_town: this.defaultFieldValue('home_town'),
        name: this.defaultFieldValue('name')
      }
    }
    this.handleInputChange = this.handleInputChange.bind(this)
    this.changeTown = this.changeTown.bind(this)
    this.changeTool = this.changeTool.bind(this)
    this.submit = this.submit.bind(this)
  }

  defaultFieldValue (type) {
    switch (type) {
      case 'favorite_tools':
        return []
      case 'home_town':
        return ''
      case 'name':
        return ''
      default:
        break
    }
  }

  mapOptions (options) {
    return options.map(option => {
      return {
        value: option,
        label: option
      }
    })
  }

  mapSelected (option) {
    return {
      value: option,
      label: option
    }
  }

  changeTown (option) {
    this.setState({
      form: update(this.state.form, {
        home_town: {$set: option.value}
      })
    })
  }

  changeTool ({target: {value}}) {
    let tools = this.state.form.favorite_tools
    tools.indexOf(value) + 1 ? tools.splice(tools.indexOf(value), 1) : tools.push(value)
    this.setState({
      form: update(this.state.form, {
        favorite_tools: {$set: tools}
      })
    })
  }

  handleInputChange ({target: {name, value}}) {
    this.setState({
      form: update(this.state.form, {
        [name]: {$set: value}
      })
    })
  }

  cleanForm () {
    Object.keys(this.state.form).forEach(field => {
      this.setState({
        form: update(this.state.form, {
          [field]: {$set: this.defaultFieldValue(field)}
        })
      })
    })
    let checkboxes = document.getElementsByClassName('checkboxes')
    for (let i = 0; i < checkboxes.length; i++) {
      checkboxes[i].checked = false
    }
  }

  submit () {
    API.postUser(this.state.form).then((res) => {
      if (res) {
        API.getUsers().then((res) => {
          this.props.changeStateProp('users', res.data, 'main')
        })
        this.cleanForm()
      }
    })
  }

  renderTools (options) {
    return (
      <ul>
        {options.map((option, index) => {
          return (
            <li key={index}>
              <input
                className='checkboxes'
                type='checkbox'
                name='tools'
                value={option}
                onClick={this.changeTool}
              />
              {option}
            </li>
          )
        })}
      </ul>
    )
  }

  render () {
    const {home_town, name} = this.state.form
    return (
      <form className='form'>
        <input
          className='text-input'
          type="text"
          name='name'
          value={name}
          placeholder='Enter your name'
          onChange={this.handleInputChange}
        />
        <SelectDropdown
          placeholder='Select item'
          options={this.mapOptions(hardcodedOption)}
          value={home_town ? this.mapSelected(home_town) : null}
          onChange={this.changeTown}
        />
        <div className='tools'>
          <span>Pick your favorite tools</span>
          {this.renderTools(hardcodedTools)}
        </div>
        <div className='submit-btn' onClick={this.submit}>SUBMIT</div>
      </form>
    )
  }
}

export default Form
