import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import Form from './Form'
import { changeStateProp } from '../../actions'

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({ changeStateProp }, dispatch)
}

const FormContainer = (connect(
  null,
  mapDispatchToProps
)(Form))

export default FormContainer
