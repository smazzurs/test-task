import axios from 'axios'
import { bindActionCreators } from 'redux'
import { showModal } from '../actions/index'
import store from '../services/store'

let bound = bindActionCreators({showModal}, store.dispatch)

export function ErrorExtractor (error) {
  if (!error.response) {
    bound.showModal({
      title: 'Error',
      message: 'There was a problem with your network. Please try again later.'
    })
    return false
  }

  if (error.response.data.message) {
    let errors = error.response.data.data
    let messages = []
    Object.keys(errors).forEach(key => {
      messages.push(errors[key])
    })
    messages = "<p>" + messages.join("</p><p>") + "</p>"
    bound.showModal({
      title: error.response.data.message,
      message: messages
    })
    return false
  } else {
    bound.showModal({
      title: 'Error',
      message: 'Something is wrong ' + error.message
    })
    return false
  }
}
