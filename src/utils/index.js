export function limitTextLength (text, maxLength) {
  if (!text) return
  if (text.length > maxLength) {
    return text.substring(0, maxLength) + '...'
  } else {
    return text
  }
}